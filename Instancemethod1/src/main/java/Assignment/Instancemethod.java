package Assignment;
public class Instancemethod {
	       public void display(){  
	            System.out.println("Hello");  
	        }  
	        public static void main1(String[] args)
	        {  
	            Instancemethod a = new Instancemethod(); // Creating object  
	            // Referring non-static method using reference  
	                P5 b = a::display;  
	            // Calling interface method  
	                b.display();  
	                // Referring non-static method using anonymous object  
	                P5 c = new Instancemethod()::display; // You can use anonymous object also  
	                // Calling interface method  
	                c.display();  
	        }   

	
	}


