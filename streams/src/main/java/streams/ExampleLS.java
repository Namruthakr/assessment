package streams;
import java.util.ArrayList;
import java.util.List;

public class ExampleLS {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		List<String> names = new ArrayList<String>();
		names.add("prethi");
		names.add("priya");
		names.add("shanthi");
		names.add("dhanya");
			
		//Using Stream and Lambda expression
		long count = names.stream().filter(str->str.length()<6).count();
		System.out.println("There are "+count+" strings with length less than 6");

	   }  

	}

