package Assesment;

import java.util.Optional;

public class OptionalClass {
	 public static void main(String[] args)
     {  
         String[] str = new String[10];        
         str[5] = "young forever ";// Setting value for 8th index  
         Optional<String> checkNull = Optional.ofNullable(str[5]);  
         if(checkNull.isPresent())
         {  // It Checks, value is present or not  
             String lowercaseString = str[5].toLowerCase();  
             System.out.print(lowercaseString);  
         }
         else  
             System.out.println("String value is not present");  
     }
}  

 